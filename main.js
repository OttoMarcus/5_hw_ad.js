import addFollowBtn from "./js/follow.js";
import addPost from "./js/addPost.js";
import addBtnListener from "./js/delete.js";
import userItem from "./js/userItem.js";

const person = document.querySelector(".users");

//          U S E R S
async function usersExecute() {
    const result = await fetch("https://ajax.test-danit.com/api/json/users");
   
    return await result.json();
}

//          P O S T S
async function postsExecute() {
    const result = await fetch("https://ajax.test-danit.com/api/json/posts");
    
    return await result.json();
}

//          C A T S
async function catsExecute() {
    const result = await fetch("https://api.thecatapi.com/v1/images/search?limit=100&api_key=live_EroMxNXoftEr8rLnBdUfN0APNyjTsuvsz1pNVVMa7dFfK8tKpTehxPdlZk79adGT");

    return await result.json();
}

async function mappingData() {
    try {
        const usersData = await usersExecute();
        const postsData = await postsExecute();
        const catsData = await  catsExecute();
        const usersMap = new Map(usersData.map(user => [user.id, user]));
        let catsMap = new Map();

        for (const [index, obj] of catsData.entries()) {
            catsMap.set(index+1 , obj);
        }



        for (let post of postsData) {
            const postId = post.id;
            const userId = post.userId;
            const user = usersMap.get(userId);

            const catUrl = catsMap.get(postId).url;

            const args = {
                title: post.title,
                body: post.body,
                name: user.name,
                mail: user.mail,
                postId: post.id,
                catUrl: catUrl
            }

            addPost(args);
        }
 
        for (let user of usersData) {
            if (user) {
                const nickname = `@${user.username}`;
                const id = user.id;
                const name = user.name;
                const list = [name, nickname];

                userItem(person, id, list);
                addFollowBtn(id);

            }
        }

        addBtnListener();
    }
    catch (error) {
       console.error("Error fetching data:", error);   
    }
}

mappingData();
 

