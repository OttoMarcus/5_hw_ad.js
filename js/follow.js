export default function addFollowBtn(id) {
    let entryPoint = document.querySelector(`.user-name-${id}`)
    let btnContainer = document.createElement("p")
    // btnContainer.classList.add("d-inline-flex",  "gap-1");
    btnContainer.innerHTML = ` <button type="button" class="btn active follow" data-bs-toggle="button" aria-pressed="true">Follow</button>`;
    entryPoint.insertAdjacentElement("afterend", btnContainer);
}

