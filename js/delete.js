export default function addBtnListener() {
    const deleteBtn = document.querySelectorAll(".btn-outline-danger");
    const postCards = document.querySelectorAll(".card");
    postCards.forEach(card => {
        card.addEventListener("click", event => {
            const del = event.target;
            if (del.className === "btn btn-outline-danger") {
                let postId = card.className.substring(10)

                deletePost(postId);

            }
        });
    })
}

async function deletePost(el) {
    let id = el.substring(3);
    const result = await fetch(`https://ajax.test-danit.com/api/json/posts/${id}`,
        {method: "DELETE"});

    if (result.ok) {
        console.log(`Delete request for post ${el} successful`);
        document.querySelector(`.${el}`).style.display = "none";
    } else {
        console.error(`Delete request for post ${el} failed`);
    }
}

 