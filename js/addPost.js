export default function addPost(args) {
    const entryPoint = document.querySelector(".posts");
    //div
    let post = document.createElement("div");
    post.classList.add("card", "mb-3", `id-${args.postId}`);
    entryPoint.appendChild(post);
    //div
    let imgContainer = document.createElement("div");
    imgContainer.classList.add("image-container");
    //img
    let imgElement = document.createElement('img');
    imgElement.src = args.catUrl;
    imgElement.classList.add("card-img-top");
    imgElement.alt = "random cat";
    post.appendChild(imgContainer);
    imgContainer.appendChild(imgElement);
    //div
    let postBody = document.createElement("div");
    postBody.classList.add("card-body");
    imgContainer.insertAdjacentElement("afterend", postBody);
    //h5
    let postTitle = document.createElement("h5");
    postTitle.classList.add("card-title");
    postTitle.textContent = args.title;
    postBody.appendChild(postTitle);
    //post-text
    let postText = document.createElement("p");
    postText.classList.add("card-text");
    postText.textContent = args.body;
    postTitle.insertAdjacentElement("afterend", postText);
    //post-name
    let username = document.createElement("div");
    username.classList.add("card-text");
    let small = document.createElement("small");
    small.classList.add("text-body-secondary");
    small.textContent = args.name;  // to add name..........!
    postText.insertAdjacentElement("afterend", username);
    username.appendChild(small);
    //post-mail
    // let mail = document.createElement("p");
    // mail.classList.add("card-text");
    let el = document.createElement("small");
    el.classList.add("text-body-secondary");
    el.textContent = args.mail;  // to add mail..........!
    // username.insertAdjacentElement("afterend", mail);
   username.appendChild(el);
    //btn
    let del = document.createElement("button");
    del.classList.add("btn", "btn-outline-danger");
    del.textContent = "Delete";
    // mail.insertAdjacentElement("afterend", del);
    username.appendChild(del);
}