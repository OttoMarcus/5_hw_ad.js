export default function userItem(entryPoint, id, list) {
    let li = document.createElement("li");
    let a = document.createElement("a");
    a.classList.add(`user-name-${id}`, "person");
    a.textContent = list.join(' ');
    entryPoint.appendChild(li);
    li.appendChild(a);
}